package com.codebound.charliespringnotes.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

public class PathVariableController {
    @GetMapping("/hello/{name}")
    @ResponseBody

    public String greeting (@PathVariable String name) {
        return "hello" + name + "!";
    }
// spring boot allows us to use path variables that are variables part of the URL request as opposed to being passed as query string
    // or as part of the request body
}
