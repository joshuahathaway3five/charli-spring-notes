package com.codebound.charliespringnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ExampleController {

    @GetMapping("/")
    @ResponseBody
    public String Hello() {
        return "Hello from spring boot";
    }


//    Notes: first step in building our application we defined a controller
//    and defined what route the controller responds to
}
